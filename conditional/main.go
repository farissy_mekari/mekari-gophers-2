package main

import "fmt"

func main(){
	age := 19

	if age < 17{
		fmt.Println("he/she does not have a citizen ID")
	}

	if age >= 6 && age <= 12{
		fmt.Println("he/she is an elementary school student")
	}

	if age > 22{
		fmt.Println("probably graduated from univeristy")
	}else if age > 18 {
		fmt.Println("probably graduated from high school")
	} else{
		fmt.Println("undetermineded")
	}

	animalCode := "F"

	switch animalCode{
	case "C":
		fmt.Println("Cat")
		break
	case "D":
		fmt.Println("Dog")
		break
	default:
		fmt.Println("Undefined")
		break
	}

	return
}