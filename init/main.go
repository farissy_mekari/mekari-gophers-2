package main

import (
	"flag"
	"fmt"
)

var numPtr *int 

func init(){
	numPtr = flag.Int("number", 0, "use for any value")
	flag.Parse()
}

func main(){
	fmt.Println("num from arguments", *numPtr)
}