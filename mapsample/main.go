package main

import "fmt"

type User struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func main(){
	users := map[int]User{
		1 : User{
			Name: "John",
		},
		2 : User{
			Name: "Cena",
		},
	}

	for k, v := range users{
		fmt.Println(k, v)
	}

	user1 := users[1]
	fmt.Println(user1)

	users[10] = User{
		Name: "Budi",
	}

	fmt.Println(users[10])

	val, ok := users[1]
	if !ok{
		fmt.Println("data is not exists")
		return
	}
	fmt.Println("data exists", val)
}