package main

import (
	"fmt"

	"bitbucket.org/farissy_mekari/mekari-gophers-2/interfacesample/user"
)

func main(){
	repo := user.NewRepository()
	user, err := repo.GetByID(8002)
	if err != nil{
		fmt.Println("unable to access repo data", err.Error())
	}
	fmt.Println(user.Name)
}