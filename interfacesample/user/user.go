package user

type User struct {
	Name   string
	Age    int
	Salary int
}
