package main

import "fmt"

func main(){
	var names [2]string
	names[0] = "John"
	names[1] = "Doe"

	fmt.Println(names)

	animals := []string{"dog", "cat"}
	fmt.Println(animals)
	fmt.Println("total animal = ", len(animals))

	cars := make([]string, 0)
	cars = append(cars, "toyota")

	fmt.Println(cars)

	animals = append(animals, "bird")
	fmt.Println(animals)
}