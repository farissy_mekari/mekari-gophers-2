package main

import (
	"errors"
	"log"
)

func main(){
	result, err := devide(1, 1)
	if err != nil{
		//error handling
		log.Fatalln("operation failed", err.Error())
		return
	}
	log.Println(result)

	res, err := getSecondData([]string{"a"})
	if err != nil{
		//error handling
		log.Fatalln("operation failed", err.Error())
		return
	}
	log.Println(res)
}

func devide(a, b int)(int, error){
	if b == 0{
		return 0, errors.New("devider should greater than 0")
	}
	return a/b, nil
}

func getSecondData(data []string)(string, error){
	if len(data) < 2{
		return "", errors.New("data length should more than 1")
	}
	return data[1], nil
}