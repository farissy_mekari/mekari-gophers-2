package main

import "fmt"

func main(){
	printHello()
	result := add(10, 11)
	fmt.Println(result)
	result = multiply("10", 2, 3)
	fmt.Println(result)

	name, _ := getData()
	fmt.Println(name)

	name, age := getPData()
	fmt.Println(name, age)
}

func printHello(){
	fmt.Println("hello from a function")
}

func add(a int, b int)int{
	return a + b
}

func multiply(s string, a, b int)int{
	fmt.Println(s)
	return a * b
}

func getData()(string, int){
	return "john", 30
}

func getPData()(name string, age int){
	name = "doe"
	age = 29
	return
}