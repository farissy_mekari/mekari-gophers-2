package main

import (
	"fmt"
)

type mathFunc func(int, int)int

type callback func(string)

func main(){
	execute(multiply, 10, 11)
	doSomething("faris", actionCallback)
}

func execute(fn mathFunc, a int, b int){
	fmt.Println(fn(a, b))
}

func multiply(a, b int)int{
	return a *b
}

func doSomething(name string, fn callback){
	fmt.Println("do something")
	newStr := fmt.Sprintf("%s___hello", name)
	fn(newStr)
}

func actionCallback(str string){
	fmt.Println("processed string=", str)
}
