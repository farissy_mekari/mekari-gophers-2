package user

import "github.com/jmoiron/sqlx"

type User struct {
	Name   string
	Age    int
	Salary int
}

func NewUserReposistory(userDB *sqlx.DB)UserRepository{
	return &userRepository{
		db: userDB,
	}
}

type UserRepository interface {
	GetUserByID(int)(*User, error)
	AddUser(User)error
}

type userRepository struct{
	db *sqlx.DB
}

func(u *userRepository)GetUserByID(id int)(*User, error){
	return nil, nil
}

func (u *userRepository)AddUser(user User)error{
	return nil
}