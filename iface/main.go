package main

import (
	"database/sql"
	"fmt"

	"bitbucket.org/farissy_mekari/mekari-gophers-2/iface/user"
	"github.com/jmoiron/sqlx"
)

const (
	uname  = "faris"
	pass   = "Cb@d7obB$m"
	host   = "talenta-staging-db.cised3xfvxir.ap-southeast-1.rds.amazonaws.com"
	dbName = "talenta_staging5"
)

func main(){
	dsnFormat := fmt.Sprintf("%s:%s@(%s:3306)/%s", uname, pass, host, dbName)
	db, err := sqlx.Connect("mysql", dsnFormat)
	if err != nil {
		fmt.Println("unable to connect to mysql", err.Error())
		return
	}

	userRepo := user.NewUserReposistory(db)
	usr, err := userRepo.GetUserByID(8002)
	if err != nil && err != sql.ErrNoRows{
		fmt.Println("unable to fetch to db", err.Error())
		return
	}
	fmt.Println(usr)

	data := getData(1)
	str, ok := data.(string)
	if !ok{ //when assertion fails
		fmt.Println("its not a string")
	}
	fmt.Println(str)
	
	if usr, ok := getData(2).(*user.User); ok{
		//when assertion true
		fmt.Println(usr)
	}
}

func getData(flag int)interface{}{
	if flag == 1{
		return "hello"
	}
	return &user.User{}
}