package employee

import "fmt"

func New(name string, age int, position string, salary float32) Employee {
	return Employee{
		Name:     name,
		Age:      age,
		Position: position,
		salary:   salary,
	}
}

type Human struct {
	Something string
}

type Employee struct {
	Human
	Name     string
	Age      int
	Position string
	salary   float32
}

func (e *Employee) GetSalaryFmt() string {
	return fmt.Sprintf("%s salary is around %f SGD", e.Name, e.salary)
}

func (e *Employee) MultiplyAgeWith2() {
	e.Age = e.Age * 2
}
