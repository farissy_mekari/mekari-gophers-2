package main

import (
	"fmt"

	"bitbucket.org/farissy_mekari/mekari-gophers-2/structsample/employee"
)

func main(){
	e1 := employee.Employee{
		Name: "John Doe",
		Age: 30,
		Position: "Engineering Manager",
	}
	
	e2 := employee.New("Fahri", 25, "Sr Software Engineer", 8000)
	fmt.Println(e1)
	fmt.Println(e2.GetSalaryFmt())
	e2.MultiplyAgeWith2()
	fmt.Println(e2.Age)

	e3 := employee.Employee{
		Human: employee.Human{
			Something: "hello",
		},
	}
	fmt.Println(e3.Human.Something)
}