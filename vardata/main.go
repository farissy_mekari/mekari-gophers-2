package main

import "fmt"

func main() {
	var aValue int
	var aName string

	aValue = 10
	aName = "John Doe"

	bValue := 11
	bName := "Thomas"

	fValue := 10.1
	gValue := int(fValue)

	fmt.Println("a values:", aValue, aName)
	fmt.Println("b values:", bValue, bName)

	fmt.Println("c values:", fValue, gValue)
}
