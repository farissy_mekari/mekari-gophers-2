package main

import "fmt"

func main(){
	var s string
	s = "john doe"
	name := getName()
	fmt.Println(name, s)

	data := getData(2)

	name, ok := data.(string)
	if !ok{
		fmt.Println("this is not a string")
		return
	}
	fmt.Println("name = ", name)
}

func getName()interface{}{
	return "john doe"
}

func getAge()interface{}{
	return 30
}

func getData(flag int)interface{}{
	if flag == 1{
		return "john cena"
	}
	return 100
}