package main

import "fmt"

type A string

type Animal struct {
	Name         string
	NumberOfFoot int
}

func main() {
	var a float32
	a = 9.8
	fmt.Println(int(a))

	var b A
	b = "Faris"
	fmt.Println(b)
}
