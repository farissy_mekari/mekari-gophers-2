package main

import "fmt"

func main(){
	for i:=0; i < 3; i++{
		fmt.Println("i = ", i)
	}

	var a int
	for{
		a++
		if a > 3{
			break
		}
	}
	fmt.Println(a)

	_, name := getData()
	fmt.Println(name)

	names := []string{"john", "doe", "thomas"}
	for _, name := range names{
		fmt.Println(name)
	}
}

func getData()(int, string){
	return 30, "Faris"
}