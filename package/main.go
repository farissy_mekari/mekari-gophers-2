package main

import (
	"fmt"

	"bitbucket.org/farissy_mekari/mekari-gophers-2/package/animal"
)

func main(){
	birdName, birdFoot := animal.GetBirdData()
	felineName, felineFoot := animal.GetFelineData()
	fmt.Println(birdName, birdFoot)
	fmt.Println(felineName, felineFoot)
}