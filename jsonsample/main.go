package main

import (
	"encoding/json"
	"fmt"
)

type User struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func main(){
	user := User{
		Name: "Faris",
		Age: 30,
	}

	bytes, err := json.Marshal(user)
	if err != nil{
		fmt.Println("unable to marshall", err.Error())
	}

	fmt.Println(string(bytes))
}