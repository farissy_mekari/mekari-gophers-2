package main

import (
	"fmt"
	"math"
)

func main(){
	a := 1
	b := 9

	fmt.Println(a + b)
	fmt.Println(a - b)
	fmt.Println(a * b)
	fmt.Println(a / b)

	fmt.Println(math.Ceil(0.9))
}